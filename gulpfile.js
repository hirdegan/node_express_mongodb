'use strict';

const gulp = require('gulp');
const nodemon = require('gulp-nodemon');
// const sync = require('browser-sync');

//watch sulla directory del client per tenerla sotto controllo
gulp.task('watch', ()=> {
    gulp.watch('client/app/**/*', (event)=> {
        console.log("Il file "+event.path+" è "+event.type+" in esecuzione");
    })
});

//semplice messaggio di avvio per il server
gulp.task('message_start', (data) => {
    console.log("SERVER IN ESECUZIONE");
    data();
});

//importazione del file del server
gulp.task('nodemon', (res)=>{
    let started = false;

    return nodemon({
        script: 'server/server.js',
        ext: 'js'
    }).on('restart', ()=>{
        if(!started){
            started = true;
            res();
        } else {
            console.log("--riavvio del server");
        }
    })
});

//eseguzione del default
gulp.task('default', gulp.parallel(
    'message_start',
    'watch',
    'nodemon'
));
