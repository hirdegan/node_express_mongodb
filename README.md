Webservices che permette l'input di oggetti definiti persona 

{
    "firstname": Stringa
    "lastname": Stringa
    "birtday": Data
    "fiscal_code": Stringa
    "description": Stringa
    "sex": array()
}

creato per server nodejs con l'utilizzo di gulp, express, moongose e mongodb

chiamate accettate all'indirizzo localhost:3030/api/person

lista dei dati
get

dati del singolo oggetto per codice fiscale
get (codice fiscale come parametro)

salvataggio dati 
post (accetta un json inviato nel body)

cerca i dati per il codice fiscalee lo rimuove. Se ci sono più risultati che corrispondono ne rimuove solo uno
delete (codice fiscale come parametro)

aggiornamento dei dati con controllo sul codice fiscale se corrisponde ne aggiorna solo 1
put (accetta un json inviato nel body)