module.exports = ()=> {

    //import dello schema
    const Person = require('./schema');

    //lista di tutti i dati
    let list = (req, res)=> {
        Person.find({}).exec()
            .then((data)=>{
                res.json(data);
            }).catch((err)=> {
                res.status(500).send(err);
            });
    };

    //insert
    let post = (req, res)=> {
        let dati = req.body;

        if ((dati.firstname == undefined) ||
            (dati.lastname == undefined) ||
            (dati.fiscal_code == undefined) ||
            (dati.sex == undefined)){
            res.status(200).send("dati non validi");
        } else {
            var person = new Person(req.body);
            person.save(req.body)
                .then((data)=> {
                    res.json(data);
                })
                .catch((err)=> {
                    res.status(500).send(err);
                });
        }
    };

    //ricerca per cf e ritorna tutti gli elementi che trova
    let get = (req, res)=> {
        Person.find({fiscal_code : req.params.dati}).exec()
            .then((data)=> {
                res.json(data);
            })
            .catch((err)=> {
                res.status(500).send(err);
            });
    };

    //rimozione di un solo elemento ricercato per cf
    let remove = (req, res)=> {
        Person.findOneAndRemove({}).exec()
            .then((data)=> {
                res.json(data);
            })
            .catch((err)=> {
                res.status(500).send(err);
            });
    };

    //cerca la persona in base al cf se non lo trova lo crea altrimenti lo aggiorna
    let update = (req, res)=> {
        Person.updateOne({fiscal_code: req.body.fiscal_code}, req.body, {upsert: true}).exec()
            .then((data)=>{
                res.json(req.body);
            })
            .catch((err)=> {
                res.status(500).send(err);
            });
    };

    return {
        list: list,
        get: get,
        post: post,
        remove: remove,
        update: update
    }
};