const express = require('express');
const path = require('path');

const controller = require(path.join(__dirname, 'controller'))();
const router = express.Router();

//risposta al client
router.get('/', controller.list);               //lista di tutti i dati
router.get('/:dati', controller.get);           //dati singolo oggetto

router.post('/', controller.post);              //inviare i dati da salvare
router.delete('/:dati', controller.remove);     //rimozione di un record
router.put('/', controller.update);             //modifica elemento

module.exports = router