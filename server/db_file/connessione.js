/**
 * connessione a mongoDB
 */

const mongoose = require('mongoose');

//connessione
mongoose.connect('mongodb://localhost:27017/archivio_persone', {
    useNewUrlParser: true,
    useUnifiedTopology: true
});

const db = mongoose.connection;
db.on(
    'error',
    console.error.bind(console, 'errore di connessione al DB')
);

db.once(
    'open', function () {
        console.log("DB CONNESSO");
    }
);

module.exports = db;