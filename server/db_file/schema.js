/**
 * definizione dello schema del documento del database
 */

const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const personSchema = new Schema({
    firstname: String,
    lastname: String,
    birtday: Date,
    fiscal_code: String,
    description: String,
    sex: []
})

const Person = mongoose.model('persones', personSchema);

module.exports = Person;