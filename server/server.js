'use strict';

const express = require('express');
const favicon = require('serve-favicon');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const methodOverride = require('method-override');
const cookieParser = require('cookie-parser');

const app = express();
const path = require('path');

const serverPort = 3030;

//connessione al db
const db = require(path.join(__dirname, 'db_file', 'connessione'));

//importazione dello schema del documento
const Person = require(path.join(__dirname, 'db_file', 'schema'))

// attivazione dei middlewares
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(methodOverride());
app.use(cookieParser());

//definizione della parte client nel caso si necessiti di importare risorse dal server
app.use(favicon(path.join(__dirname, '..', 'client', 'favicon.ico')));

//definire l'accesso pubblico alla directory del client
app.use(express.static(path.join(__dirname, '..', 'client')));


//dati di risorsa per il database
app.use('/api/person', require(path.join(__dirname, 'db_file')));


//nel caso di chiamata del server rimanda alla directory client
app.get('/', (req, res)=> {
    //res.send('<h1>Accesso non autorizzato</h1>');
    res.sendFile(path.join(__dirname, '..', 'client'));
})

//definizione del server per l'esecuzione
const server = app.listen(serverPort, ()=> {    
    console.log(server.address());
    const host = server.address().address;
    const port = server.address().port;
    console.log('Server in ascolto all\'indirizzo http://%s:%s', host, port);
});